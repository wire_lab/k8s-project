## (一) 安裝
安裝步驟全部包在一個shell檔
```
$ sudo chmod +x install.sh
$ ./install.sh
```

## (二)建立 Cluster
建立指令如下方區塊，用[]框起來的地方是optional
```
$ sudo kubeadm init [--apisrver-advertise-address: <your ip address>]
```

設定kubectl讓regular user可以連到kubernetes cluster
```
$ make -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
```

在設定好網路add-on之前，任何application還不能被部署，kubernetes提供多種add-on的選擇，可參考[官網](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#pod-network)
```
kubectl apply -f <add-on.yaml>

這裡以flannel為例
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml
```

## (三)建立 worker node
1. 安裝kubernetes需要的套件，請參考（一）
2. 從master上產生token，然後透過token資訊把Node加入到Cluster
```
產生token
$ kubeadm token create -print-join-command
加入Cluster
$ kubeadm join --token
```

## (四) 運行Kubernetes最小單位 -- Pod
所有的docker container都是運行在kubernetes的最小單位Pod，當我們建立好Pod時，Kubernetes會自動啟動container來提供服務，操作步驟：
1. 撰寫yaml設定檔，請參考application/pod-example.yaml
2. 建立Pod
```
$ kubectl create -f pod-example.yaml
```

## (五) 有效管理Container
為了避免某一個Pod crash而終止服務，透過Deployment能自動偵測Pod的健康狀態，當crash發生時會自動創建一個新的Pod，並且能支援同一個服務設定多個Pod來增加服務的可靠性。，
此外，要讓Cluster外部的user可以連線到內部服務，需使用Service把位於Pod上的targetPort map到Node上的NodePort，如此，使用者可透過 <Node IP>:<NodePort>來訪問群集內的Pod

Deploy和Service設定方式可以合在一個yaml設定檔，亦可分開寫，撰寫方式請參考application/web.yaml
所有資源的建立方式都是透過create
```
$ kubectl create -f web.yaml
```

若要建立不同的service，如vsftpd server的話，執行方式，可參考[原始出處](https://github.com/aledv/kubernetes-ftp)
```
In application/vsftpd directory
$ cd applicatoin/vsftpd
$ kubectl create -f mount-rue/task-pv-volume.yaml
$ kubectl create -f mount-rue/task-pv-claim.yaml
$ kubectl create -f vsftpd-deployment.yaml
$ kubectl create -f vsftpd-service.yaml
```

資源建立完以後，由get或describe取得創見後的資訊
```
$ kubectl get pods  # get the pod name, and the status
$ kubectl describe pod <pod-name> # get the more completely information
```
同樣地，若要取得deployment和service資訊，將上述的pod改為deployment或service即可

---

## (六) 部署NGINX Ingres Controller
Ingress Contronller能設定Kubernetes使用外部的Load Balancer，這部份NGINX本身有提供[完整程式碼與步驟說明](https://github.com/nginxinc/kubernetes-ingress/blob/master/docs/installation.md)，此project所用為其中的部份，設定方式：
```
In nginx-ingress directory
$ cd nginx-ingress
$ kubectl create -f common/ns-and-sa.yaml
$ kubectl create -f common/default-server-secret.yaml
$ kubectl create -f common/nginx-config.yaml
$ kubectl create -f rbac/rbac.yaml
$ kubectl create -f deployment/nginx-ingress.yaml
$ kubectl create -f service/nodeport.yaml
```

## (七) 建立Ingress rule
設定Ingress rule讓不同的request連至對應的service
```
$ kubectl create -f ingress.yaml
```

完成後，即可用host name或IP連線對應的服務

